import numpy as np
import pandas as pd
import warnings
import sys

warnings.catch_warnings()
warnings.simplefilter("ignore")


def powerset(seq):
    """
    Returns all the subsets of this set. This is a generator.
    """
    if len(seq) <= 1:
        yield seq
        yield []
    else:
        for item in powerset(seq[1:]):
            yield [seq[0]]+item
            yield item


def get_groups_for_training(df, X, y_columns, splitby_column, split_value,
                            splitby_column_filter_values,
                            skip_when_no_data=True):
    """Generator of tuples of the form:

        (X_train, X_test, y_train, y_test, grp, y_column)

    Function splits data in two sets using 'splitby_column' and 'split_value'.
    The set with values for 'splitby_column' equal to 'split_value' correspond
    to the test partition (X_test, y_test), whereas the set that doesn't
    contain the data is used for training (X_train, y_train). The values for
    y_train and y_test on each generated tuple correspond to values coming from
    iterating over 'y_columns' (a tuple for each value).

    If a 'splitby_column_filter_values' list is given, for every item in it (a
    list of values from the splitby_column) a tuple is generated. Using each
    item in this list, before yielding a tuple, the train and test partitions
    are filtered so that only rows with values for 'splitby_column' contained
    in the item (a list) are returned. All other rows are ignored and not
    included. If 'splitby_column_filter_values' is an empty list, no filtering
    is performed and the entire list of values is used. The current group of
    values used is returned in the 'grp' value.

    The number of tuples returned is:

        len(y_columns) * len(splitby_column_filter_values)

    Example:

        get_groups_for_training(
            df, X=['f1', 'f2', 'f3'], ['y1', 'y2'], 'grouping_col', 'g1',
            [
                ['g2', 'g3'], ['g3'], ['g4']
            ]
        )

    The above generates 6 tuples. X_train and X_test correspond to values
    from df[['f1', 'f2', 'f3']] (split/filtered based on the other args of this
    function).

      (X_train, X_test, df[y1](train), df[y1](test), ['g2', 'g3'], 'g1', 'y1')
      (X_train, X_test, df[y1](train), df[y1](test), ['g3'], 'g1', 'y2')
      (X_train, X_test, df[y1](train), df[y1](test), ['g2'], 'g1', 'y2')

      (X_train, X_test, df[y2](train), df[y2](test), ['g2', 'g3'], 'g1', 'y1')
      (X_train, X_test, df[y2](train), df[y2](test), ['g3'], 'g1', 'y2')
      (X_train, X_test, df[y2](train), df[y2](test), ['g2'], 'g1', 'y2')
    """
    df_without = df[df[splitby_column].str.contains(split_value) == False]
    df_with = df[df[splitby_column].str.contains(split_value) == True]

    X_test = df_with[X]

    if len(X_test) == 0:
        raise Exception(
            'No test data after splitting on {}'.format(split_value))

    for values_include in splitby_column_filter_values:
        # ignore empty list
        if len(values_include) == 0:
            continue

        # get relevant data (ignore values not in current group)
        v = [split_value] + values_include
        q = '|'.join([splitby_column+'.str.startswith("'+x+'")' for x in v])
        df_filtered_without = df_without.query(q)

        X_train = df_filtered_without[X]

        if len(X_train) == 0:
            raise Exception('No train data after splitting on {}'.format(v))

        # get model for each app (if no data for app, we skip it)
        for y_col in y_columns:

            y_train = df_filtered_without[y_col]
            y_test = df_with[y_col]

            if np.mean(y_test) == 0.0 and skip_when_no_data:
                print('No data [{}, {}]. Skipping'.format(split_value, y_col))
                continue

            yield X_train, X_test, y_train, y_test, values_include, y_col


def load_dataset(path_to_results, test_list=None, ignore_bench=None,
                 machines_include=None, machines_exclude=None,
                 repetition_as_datapoint=True):
    """Loads the dataset and returns a 2-element tuple. The first element
    corresponds to the data "as is" from the CSV file, plus some cleanups. This
    dataframe contains one benchmark result per row. The second element
    contains the transpose (or pivoted) dataframe, where every row is a machine
    and every column corresponds to a feature, either a microbenchmark result
    or the result of a performance test for an application. If 'ignore_bench'
    is given, the data is filtered by the 'benchmark' column, i.e. all data
    points where 'benchmark == @ignore_bench' are removed. If 'machines'
    is given (a non-empty list is passed), only data for machines on the list,
    is loaded, using the query 'machine in @machines'. If
    'repetition_as_datapoint' is True and the 'repetition' column exists, each
    repetition is treated as a distinct data point, otherwise the mean of the
    [machine,test] group is taken as the value for the 'result' column.
    """
    results = pd.read_csv(path_to_results)

    # check expected columns
    for c in ['test', 'machine', 'result', 'benchmark']:
        if c not in results.columns:
            print("Expecting one column named '{}'".format(c))
            sys.exit(1)

    # if a test is named 'null' it'll loaded as a null string, so let's fix it
    results['test'] = results['test'].apply(
        lambda x: 'null' if pd.isnull(x) else x
    )

    if test_list and ignore_bench:
        raise Exception("Cannot use test_list and ignore_bench at same time")
    if machines_include and machines_exclude:
        raise Exception(
            "Cannot use machines_include and machines_exclude at same time.")

    # filter data
    if test_list:
        results = results.query("test in @test_list")
    if ignore_bench:
        results = results.query("benchmark != @ignore_bench")
    if machines_include:
        results = results.query("machine in @machines_include")
    if machines_exclude:
        results = results.query("machine not in @machines_exclude")

    if len(results['machine'].unique()) < 2:
        raise Exception("Not enough machines in dataset.")

    if not repetition_as_datapoint:
        grouped = results.groupby(['machine', 'test'])
        results['result'] = grouped['result'].transform('mean')

    if 'repetition' in results.columns:
        # treat every repetition as a new data point

        # TODO: if dataset contains multiple executions (e.g. a timestamp col),
        #       we should first group results in order to create subsets of
        #       executions, and create uniqueness by
        #       (machine, execution_timestamp, repetition)
        results['machine_id'] = results.apply(
            lambda x: x['machine'] + '-' + str(x['repetition']), axis=1)
    else:
        results['machine_id'] = results['machine']

    # pivot table to obtain features, where each test is a feature
    data = results.pivot_table(
        index=["machine_id", "test"],
        values=['result'],
        fill_value=0,
        aggfunc=np.mean
    ).unstack().reset_index()

    # flatten the index
    cols = data.columns.get_level_values(1).values
    cols[0] = 'machine_id'
    data.columns = cols
    data.fillna(0, inplace=True)

    return results, data


def get_feature_names(df, bench_name):
    "Returns name of features (test names) for a given benchmark"
    query = 'benchmark == "{}"'.format(bench_name)
    return list(pd.Index(df.query(query)['test'].unique()))

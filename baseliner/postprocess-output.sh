#!/bin/bash
set -e -x

OUTDIR="$1"
CSVOUT="$2"

# stressng
docker run --rm -v $OUTDIR/benchmark:/data/benchmark \
  ivotron/json-to-tabular:v0.0.5 \
    --jqexp '. | .metrics | .[] | [.stressor, ."bogo-ops-per-second-real-time"]' \
    --filefilter '.*stressng.*yml' \
    ./ >> $CSVOUT

# likwid
docker run --rm -v $OUTDIR/benchmark:/data/benchmark \
  ivotron/json-to-tabular:v0.0.5 \
    --jqexp '[leaf_paths as $path | select( ($path | join(".")) | contains("mbyte_per_s") ) | {"key": $path | join("."), "value": getpath($path)}] | .[] | [.key, (.value | tonumber)]' \
    --filefilter '.*output.*yml' \
    ./ >> $CSVOUT

# mysql
docker run --rm -v $OUTDIR/benchmark:/data/benchmark \
  ivotron/json-to-tabular:v0.0.5 \
    --jqexp 'to_entries | map([.key, .value]) | .[] ' \
    --filefilter '.*mysqlslap.*json' \
    ./ >> $CSVOUT

# redis
docker run --rm -v $OUTDIR/benchmark:/data/benchmark \
  ivotron/json-to-tabular:v0.0.5 \
    --filefilter '.*redisbench.*.csv' \
    ./ >> $CSVOUT

# runtime-based ones
for w in cloverleaf comd hpccg lulesh miniamr miniaero minife pybench scikit-learn silt ssca zlog ; do
  docker run --rm -v $OUTDIR/benchmark:/data/benchmark \
    ivotron/json-to-tabular:v0.0.5 \
      --filefilter ".*$w.*runtime" \
      --shex "sed 's/\(.*\):\(.*\):\(.*\)/\1 \2 \3/' | awk '{ print \"$w,\"((\$1 * 3600) + (\$2 * 60) + \$3) }'" \
      ./ >> $CSVOUT
done

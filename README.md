To run notebooks:

```bash
docker run --rm -ti \
  -p 8888:8888 \
  -v "$PWD":/workspace \
  -w /workspace \
  continuumio/anaconda3:2020.02 jupyter lab \
    --ip 0.0.0.0 \
    --no-browser \
    --allow-root \
    --NotebookApp.token=''
```

Then go to <http://localhost:8888> and browse the 
[`notebook/`](./notebook) folder.

